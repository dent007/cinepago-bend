package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Tarjeta;
import um.prog2.service.ITarjetaService;
@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/tarjeta") // Indica una ruta de acceso url
public class TarjetaController {

	@Autowired
	private ITarjetaService tarjetaService;
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Tarjeta>> listarTarjetasTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Tarjeta> tajetas = new ArrayList<>();
		try {
			
			tajetas=tarjetaService.listar();

		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Tarjeta>>(tajetas, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		
		return new ResponseEntity<List<Tarjeta>>(tajetas, HttpStatus.OK);
	}

	@GetMapping( value = "/listar/{idTar}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Tarjeta> listarTarjetaPorId(@PathVariable("idTar") Integer idTar) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Tarjeta tarjeta = new Tarjeta();
		try {
			tarjeta=tarjetaService.listarId(idTar);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Tarjeta>(tarjeta, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Tarjeta>(tarjeta, HttpStatus.OK);
	}
	@DeleteMapping( value = "/eliminar/{idTar}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarTarjetaPorId(@PathVariable("idTar") Integer idTar) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			tarjetaService.eliminar(idTar);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarTarjeta(@RequestBody Tarjeta tar) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=tarjetaService.registrar(tar); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarTarjeta(@RequestBody Tarjeta tar) {
		
		//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=tarjetaService.modificar(tar); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
}
