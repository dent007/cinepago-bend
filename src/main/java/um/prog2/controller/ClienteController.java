package um.prog2.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Cliente;
import um.prog2.service.IClienteService;
@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/cliente") // Indica una ruta de acceso url
public class ClienteController {
	
	@Autowired
	private IClienteService clienteService;
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> listarClientesTodos() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Cliente> clients = new ArrayList<>();
		try {
			clients=clienteService.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Cliente>>(clients, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Cliente>>(clients, HttpStatus.OK);
	}

	@GetMapping( value = "/listar/{idPag}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> listarClientePorId(@PathVariable("idPag") Integer idPag) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Cliente pago = new Cliente();
		try {
			pago=clienteService.listarId(idPag);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Cliente>(pago, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Cliente>(pago, HttpStatus.OK);
	}
	@DeleteMapping( value = "/eliminar/{idPag}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarClientePorId(@PathVariable("idPag") Integer idPag) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);

			clienteService.eliminar(idPag);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		//System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+ resultado );

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarCliente(@RequestBody Cliente pag) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=clienteService.registrar(pag); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarCliente(@RequestBody Cliente pag) {
		
		//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=clienteService.modificar(pag); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	@GetMapping( value = "/listarPageable", produces = MediaType.APPLICATION_JSON_VALUE)
	//public ResponseEntity<Page<Cliente>> listarPaginas(@PageableDefault(value=5, page=0)  Pageable pageable) {
	public ResponseEntity<Page<Cliente>> listarPaginas( Pageable pageable) {
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Page<Cliente> clients = null;
		try {
			clients=clienteService.listAllByPage(pageable);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Page<Cliente>>(clients, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Page<Cliente>>(clients, HttpStatus.OK);
	}
}
