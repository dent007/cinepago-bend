package um.prog2.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="tarjeta")
public class Tarjeta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int tarjeta_id;
	
	@Column(name="numero", nullable=false)
	private String numero;

	@ManyToOne
	@JoinColumn(name="cliente_id",nullable=false)
	private Cliente cliente;

	@Column(name="saldo", nullable=false)
	private int saldo;
	
	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime update;
	
	@Column(name="tipo", nullable=false)
	@Enumerated(EnumType.STRING)
	private Tipo tipo;
	/*
	@JsonIgnore
	@OneToMany(mappedBy="tarjeta", cascade= { CascadeType.PERSIST, //CascadeType.ALL
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY, 
			orphanRemoval = true)	
	private List<Pago> pago;
	*/
	public enum Tipo {CREDITO,DEBITO}
	
	
	public Tarjeta() {
	}
	/*
	public List<Pago> getPago() {
		return pago;
	}

	public void setPago(List<Pago> pago) {
		this.pago = pago;
	}*/
	public int getTarjeta_id() {
		return tarjeta_id;
	}

	public void setTarjeta_id(int tarjeta_id) {
		this.tarjeta_id = tarjeta_id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
/*
	@Override
	public String toString() {
		return "Tarjeta [tarjeta_id=" + tarjeta_id + ", numero=" + numero + ", cliente=" + cliente + ", saldo=" + saldo
				+ ", created=" + created + ", update=" + update + ", tipo=" + tipo + ", pago=" + pago + "]";
	}
*/

}
