package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cliente_id;
	
	@Column(name="documento",nullable=false)	
	private int documento;
	
	@Column(name="apellido",nullable=false)
	private String apellido;
	
	@Column(name="nombre",nullable=false)
	private String nombre;
	
	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime created;

	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime update;
	
	
	/*@OneToMany(mappedBy="cliente", cascade= { CascadeType.PERSIST, //CascadeType.ALL
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY, 
			orphanRemoval = true)		
	@JsonIgnore
	private List<Tarjeta> tarjetas;
*/
	public Cliente() {
	}

	public int getCliente_id() {
		return cliente_id;
	}

	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}

	public int getDocumento() {
		return documento;
	}

	public void setDocumento(int documento) {
		this.documento = documento;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}
	/*
	public List<Tarjeta> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<Tarjeta> tarjetas) {
		this.tarjetas = tarjetas;
	}

	@Override
	public String toString() {
		return "Cliente [cliente_id=" + cliente_id + ", documento=" + documento + ", apellido=" + apellido + ", nombre="
				+ nombre + ", created=" + created + ", update=" + update + ", tarjetas=" + tarjetas + "]";
	}
	
	*/
	}
