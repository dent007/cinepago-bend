package um.prog2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinePagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinePagoApplication.class, args);
	}

}

