package um.prog2.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
@Component
@Order(Ordered.HIGHEST_PRECEDENCE) // indica mayor importancia,ntes de protegerlos debe comunicarse con ellos
public class Cors implements Filter{
	
	@Override
	public void init(FilterConfig filterConfig)throws ServletException{
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
			HttpServletResponse response = (HttpServletResponse) res;
			HttpServletRequest request = (HttpServletRequest) req;
			/*permite el paso de estas peticiones de cualquier URL*/
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
			response.setHeader("Access-Control-Max-Age", "3600");
			
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with, authorization,Authorization, Content-Type,credential,X-XSRF-TOKEN");																			
			response.addHeader("Access-Control-Expose-Headers", "xsrf-token");
	        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
			
	        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
	            response.setStatus(HttpServletResponse.SC_OK);
	            
	        }else {
	        	chain.doFilter(req,res);	
	        }
	 
	        // pass the request along the filter chain
	        
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
