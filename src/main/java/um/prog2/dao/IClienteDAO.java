package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Cliente;
@Repository
public interface IClienteDAO extends JpaRepository<Cliente, Integer>{
//public interface IClienteDAO extends PagingAndSortingRepository<Cliente, Integer>{

}
