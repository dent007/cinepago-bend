package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Tarjeta;
@Repository
public interface ITarjetaDAO extends JpaRepository<Tarjeta, Integer>{

}
