package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.ITarjetaDAO;
import um.prog2.model.Tarjeta;
import um.prog2.service.ITarjetaService;
@Service
public class TarjetaServicioImpl implements ITarjetaService{

	@Autowired
	private ITarjetaDAO tarjetaDao;
	
	@Override
	public int registrar(Tarjeta tar) {
		int salida=0;
		salida = tarjetaDao.save(tar) !=null ? tar.getTarjeta_id():0;
		return salida > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Tarjeta tar) {
		int salida=0;
		salida = tarjetaDao.save(tar) !=null ? tar.getTarjeta_id():0;
		return salida > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idTar) {
		tarjetaDao.delete(idTar);
		
	}

	@Override
	public Tarjeta listarId(int idTar) {
		
		return tarjetaDao.findOne(idTar);
	}

	@Override
	public List<Tarjeta> listar() {
		
		System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
		System.out.println("XXXX" + tarjetaDao.findAll().size());
		System.out.println("ddddddddddddddddddddddddddddddddddd");

		return tarjetaDao.findAll(); 
	}

}
