package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import um.prog2.dao.IClienteDAO;
import um.prog2.model.Cliente;
import um.prog2.service.IClienteService;

@Service
public class ClienteServicioImpl implements IClienteService{

	@Autowired
	private IClienteDAO clienteDao;
	
	@Override
	public int registrar(Cliente clie) {
		int salida=0;
		salida=clienteDao.save(clie)!=null ? clie.getCliente_id():0;
		return salida > 0 ? 1:0;
	}

	@Override
	public int modificar(Cliente clie) {
		int salida=0;
		
		salida=clienteDao.save(clie)!=null ? clie.getCliente_id():0;
		return salida > 0 ? 1:0;
	}

	@Override
	public void eliminar(int idCli) {
		clienteDao.delete(idCli);
		
	}

	@Override
	public Cliente listarId(int idCli) {
		// TODO Auto-generated method stub
		return clienteDao.findOne(idCli);
	}

	@Override
	public List<Cliente> listar() {
		// TODO Auto-generated method stub
		return clienteDao.findAll();
		//return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	public Page<Cliente> listAllByPage  (Pageable pageable) {
		return clienteDao.findAll(pageable);
	}

}
