package um.prog2.service;

import java.util.List;

import um.prog2.model.Tarjeta;

public interface ITarjetaService {
	int registrar(Tarjeta tar);
	int modificar(Tarjeta tar);
	void eliminar(int idTar);
	Tarjeta listarId(int idTar) ;
	List<Tarjeta> listar(); 

}
