package um.prog2.service;

import java.util.List;

import um.prog2.model.Pago;

public interface IPagoService {
	int registrar(Pago pag);
	int modificar(Pago pag);
	void eliminar(int idPag);
	Pago listarId(int idPag) ;
	List<Pago> listar(); 


}
