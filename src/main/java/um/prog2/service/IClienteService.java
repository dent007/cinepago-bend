package um.prog2.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import um.prog2.model.Cliente;

public interface IClienteService {
	int registrar(Cliente clie);
	int modificar(Cliente clie);
	void eliminar(int idCli);
	Cliente listarId(int idCli) ;
	List<Cliente> listar();
	Page<Cliente> listAllByPage(Pageable pageable); 
}

